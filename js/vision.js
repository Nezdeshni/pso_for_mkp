/*
var pso_view={};

	pso_view._construct=

	function(){
*/

		function createFlatten(){
			
			var svg; 
			var ds;

			var color = d3.scaleLinear()
				.domain([0, 0.5, 1])
				.range(["red", "yellow", "green"]);
			
			function repaint(){
			var w = 960,
				h = 500,
				z = ds.cs,
				x = ds.dim().col,
				y = ds.dim().row;
				
				svg = container();
				
				var g = svg.selectAll("rect")
					.data(d3.range(ds.sf[0].length).map(function(e,i){return {v:e,dx:(e % x) * z,dy:Math.floor(e / x) * z}}))
					.enter().append("g")
					.attr("x", function(d){ return d.dx})
					.attr("y", function(d){ return d.dy})
					.attr("transform", translate)
					.attr("prob", function(d){return ds.sf[0][d.v]})
					.attr("bn",function(d){return d.v});
					
					
					g.append("rect")
					.attr("width", z)
					.attr("height", z)
					.style("fill", 	cl)
					.on("mouseover", mouseover);
					
					g.append("text")
					.attr("text-anchor", "middle")
					.attr("dominant-baseline", "central")
					.attr("x", function(d){ return z/2})
					.attr("y", function(d){ return z/2})
					.text(function(d){return d.v})
					.style("fill", "black"); ;
					
					
					function container(){
						d3.select("#pso").remove();
						return d3.select("body").append("svg")
						.attr("width", w)
						.attr("height", h)
						.attr("id","pso");
					}
					function translate(d) {
					  return "translate(" + d.dx + "," + d.dy + ")";
					}
					function mouseover(d,n,a){
						//d3.select(a[d]).attr("title",d);
					}
			}

			
			function getDs(){
				return ds;
			}
			
			function setDs(x){
				ds=x;
				ds.sf=x[x.tsLabel]||x.sf;
				ds.n=ds.sf.length;
				repaint();
			}
			
			function nextDsItem(param){
				//console.dir(svg.selectAll("rect"))
				ds.i++;
				//param.animated=param.animated==undefined?false:param.animated;
				//console.log(ds.i)
				param=param||{};
				return setDsItem(false,param);
			}
			function setDsItem(x,param){
				ds.i=x?x%ds.n:(ds.i)%ds.n;
				rcolor(ds.sf[ds.i],param);
				return ds.i;
			}
			
			function cl(d){
				return color(d3.select(this).attr("prob"));
			}
			
			function en(d){
				if(d.v==ds.sf[0].length-1){
					if(ds.i<(ds.n-1)){nextDsItem(animation_param);}
					else{
						if(inf_anim){
							nextDsItem(animation_param);
						} else {	
							ds.i=0;
							console.log("last frame was reached") 
							}
					}
				}
			}
		
			function rep(){
				svg.selectAll("rect").attr("prob", function(d){return d.v})
			}

			function rcolor(a,param){
				h=param.animated?en:null;
				duration = param.duration||500;
				svg.selectAll("rect")
					.attr("prob", function(d){return a[d.v]})
					.transition()
					.duration(duration)
					.style("fill",cl)
					.on("end",h);
					return svg;
			}
			
			function anim(param){
				var fromFrame=param.fromFrame||0;
				param.animated=true;
				animation_param=param;
				setInf(param.inf);
				return setDsItem(fromFrame,param);
			}
			
			var inf_anim=false;
			var animation_param;
			function setInf(b){inf_anim=b||false}
			
			return {	
				'setData':setDs,
				'nextFrame':nextDsItem,
				'getDs':getDs,
				'anim':anim
			}
		}
/*			

	}
	this.createFlatten=createFlatten;
}
*/


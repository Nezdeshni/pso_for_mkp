var RND_DIGIT = function(VALUE){return Math.floor(Math.random()*(VALUE-1)+1)};
var RND_NORM = function(VALUE){return ~~(-(Math.log(Math.random())*VALUE))};
var RND_VALUE = function(VALUE){return Math.random()*VALUE};
var ARRAY_OF = function(len,v){return Array.apply(null, {length: len}).map(function(){return typeof (v) !== 'function' ? v:v()})};

class KpItem {
	constructor(o)
	{
		this.coast=o;
		this.income=0;
		this.id=0;
		this.owner=null;
		this.selected=false;
		return this;
	}

	isPlaceable(){
		if(this.selected){return false}
		for (var c in this.coast) {
			if(this.owner.amount[c]-this.coast[c]<0){return false}
		}
		return true;
	}

	put(){
		if(!this.isPlaceable()){return false}
		for(var c in this.coast){
			this.owner.amount[c]-=this.coast[c];
		}
		this.owner.goods+=this.income;
		this.owner.state[this.id]=1;

		this.selected=true;
		return this.owner.goods;
	}
	remove(){
		if(!this.selected){return -1}
		this.owner.state[this.id]=0;
		for (var c in this.coast) {
			this.owner.amount[c]+=this.coast[c];
		}
		this.owner.goods-=this.income;
		this.selected=false;
		return this.owner.goods;
	}
}

							
function Kanspak(s)
		{	s = s||{};
			this.amount		=     s['amount']||{};
			this.content		=	  [];
			this.state		=	  [];
			this.goods		=	   0;
			
			return this;
		};
		
Kanspak.prototype.description			=function(){
				var s=""
				var escape="\n";
				//s+="amount of resources: "+this.amount+escape;
				//s+="content for selection: "+this.content.len+" elements "+escape;
				return console.dir(this);
}

Kanspak.prototype.saveGivenSet			=function(key)	{
				var cont = this;
				this.free();
				var jo = {
						stuff : JSON.stringify(this.content.map(function(e,i){e.owner = null; return e})),
						kp	   :  JSON.stringify(this.amount)
					}
				localStorage.setItem(key, JSON.stringify(jo));
				this.content.map(function(e, i){e.owner = cont; return e});
				return this;
}



Kanspak.prototype.loadGivenSet			=function(key)	{
				var cont = this;
				var stored_o =  localStorage.getItem(key);
				if (stored_o === null || stored_o === undefined) {
					return value = "";
				}
				this.free();
				this.content=JSON.parse(JSON.parse(stored_o).stuff).map(function(e, i){var x = Object.setPrototypeOf(e,KpItem.prototype); x.owner = cont; return x});
				this.state=ARRAY_OF(this.content.length,0);
				this.amount = JSON.parse(JSON.parse(stored_o).kp);
				return this;
}

Kanspak.prototype.createItem			=function(o, g)	{
												for (var p in o) {	
													if(!this.amount[p]){return false}
												}
												var ord = new KpItem(o);
												ord.income=g;
												ord.id=this.content.length;
												ord.owner=this;
												this.content.push(ord);
												this.state.push(0);
												return true;
											};
																						
Kanspak.prototype.putById			=function(id){
										return this.content[id].put();
}
Kanspak.prototype.remById			=function(id){
										return this.content[id].remove();
}


Kanspak.prototype.availableItems	=function(){
										var as;
										as = this.content.reduce(function(p, c, i){if(!c.selected){p.push(c)} return p},[])
										return as;
}

Kanspak.prototype.free				=function(){
										this.content.map(function(e, i){e.remove()});
										
}

Kanspak.prototype.estimateSolution =function(a){
	if(a.length!==this.state.length){return false;}
	var tmp_a=this.state;
	this.free();
	var i=0;
	var v=0;
	while(i<a.length){
		if(a[i]!==0){
			var s=this.putById(i);
			if(s){v=s}else{
					return false;
				}
		}
		i++
	}
	return v
}


Kanspak.prototype.extractRand		=function(l){
	var o={};
	for(var p in l){o[p]=RND_NORM(l[p])}
	return o;
}


Kanspak.prototype.randomItems		 =function(n, avVol, maxIncome){
	var t_vol = {};
	while(n>0){
		var m=1
		this.createItem(this.extractRand(avVol),m*RND_NORM(maxIncome));
		n--;
	}
	return this.content;
}
Kanspak.prototype.randomState		 =function(){
	this.free();
	var f=true;
	while(f){
		var availableItems=this.availableItems();
		if(availableItems.length===0){break};
		f=availableItems[RND_DIGIT(availableItems.length)].put();
	}
	return this.goods;
}


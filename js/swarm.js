var pso={};

pso._construct=

function(){
	LOCAL_AREA_SIZE=8;
	GSP_MLP=1;
	//Problem param
	//number of swarm particles used to solve optimization problem
	const DEFAULT_SWARM_SIZE = 30;
	//number of optimization problem dimentions (length of binary vector which represent solution)
	const DEFAULT_DIMENTIONS = 8;

	//Model behavior param

	/*swarm dencity threshold, when swarm.gravity value become higher then this parameter, swarm population
		would be partialy replaced. High dencity indicates proximity of all swarm solutions to best solution
		ever obtained by swarm, so optimization process would stuck in corresponding extremum. Values of gravity
		may change from 0 to 1, where 
			0 mean all solutions presented by swarm particles differ from best known
			solution with each component (no matches in best solution components any solution 
			stored by particles),
			1 mean all particles store fully identical solutions (have equal positions), and this solution fully
			matches with best solution ever found by swarm
			0.x mean that 0.x components of particles solutions matches with best solution ever found by swarm
		
		*/
	const DEFAULT_GRAVITY_MAX = 0.999;
	/*Portion of new particles in new population, generated when algorithm stucks in fouded extremum*/
	const DEFAULT_NEXTGEN_PORTION  = 99;
	//attractive power of swarm best known position
	const DEFAULT_LOCAL_BEST_MPL = 3;//3
	//attractive power of self best known position
	const DEFAULT_GLOBSL_BEST_MPL = 5;//5
	//attractive power of self current position
	const DEFAULT_MODEL_INERCIA = 1;//1

	//Finalization criteria
	const DEFAULT_TARGET_FIT = 56;
	const DEFAULT_MAX_TURNS = 64000;
	//Initial state generation
	//Ititial position vector of all particles contains COMPONENT_PROB*(position vector length) values=1, other values=0
	const COMPONENT_PROB = 0.5;
	//Probability of random invertion of swarm best known position component in case of algorithm stucks in fouded extremum
	const MUTATION_PROB = 0.0001;
	//Random invertion of swarm best known position component, could seriously impair effectivness of optimization
	//so it would be applied only if IS_MUTABLE=true (mutation is disabled by default)

	//function GOSSIP_TOGGLE(){USE_GOSSIPS=!USE_GOSSIPS; return ("Gossip flag state: "+USE_GOSSIPS);}

	//Default callback function for initialization new particle velosities
	function INITIAL_VELOSITY(){return 0.05+RND_VALUE(0.9)}

	//Default callback for estimating solution fit, it return number of 1 in solution vector.
	function DEFAULT_FIT_FUNCTION(a){return a.reduce(function(p,c){return p+c},0)}
	//Default rule of solution fit comparation (if new fit value higher then old fit value return true - higher fit values=better solution)
	function IS_QALITY_BETTER(old_fit,new_fit){return old_fit<new_fit}
	//Normalization formula translate velosity values (v) to 0..1 interval
	function NORMALIZE(v){return 1/(1+Math.exp(-v))}
	//Default function called when swarm find solution provides better quality than all previous 
	function DEFAULT_LMIN_CALLBACK(s){
		s.debugInf.steps=s.debugInf.steps||[];
		s.debugInf.steps.push({t:(DEFAULT_MAX_TURNS-s.max_turns),f:s.bestGF})
		s.debugInf.stepsTotal = s.debugInf.steps.length;
		//console.log((DEFAULT_MAX_TURNS-s.max_turns)+": "+s.bestGF+" /g: "+s.gravity)	
	}
	//Default function called when swarm dencity exceed swarm.g_max value
	function DEFAULT_MAX_DENCITY_CALLBACK(s){
		//console.log("next gen: g= "+s.gravity);
		s.nextGen(s.nextGenPortion);
		return s;
	}
	//Return array of length len, filled with values v. If v is function, array elements would be filled with results of calling v.
	ARRAY_OF = function(len,v){return Array.apply(null, {length: len}).map(function(){return typeof (v) !== 'function' ? v:v()})};
	//Return random float number in range 0..VALUE
	RND_VALUE = function(VALUE){return Math.random()*VALUE};
	//Return value 1 with probability p, or value 0 with probability 1-p
	GET_RELIZATION = function(p){return RND_VALUE(1)<p ? 1:0};
	//Return array of length len filled by binary digits. Each digit is randomly generated, probability of value 1 is equals to p;
	RND_BINARY_ARRAY = function(len,p){return Array.apply(null, {length: len}).map(function(){return GET_RELIZATION(p)})};
	ARRAY_OF_RND_P = function(len,p){return Array.apply(null,{length:len}).map(function(){return RND_VALUE.call(1,p)})};
	
	
	
	
	/******************		UNUSED FOR A WHILE BEGIN	**************************/
	/**************************************************************************/
	ARRAY_UNFLATTING = function(inp){	
										var l=~~Math.sqrt(inp.length); 
										var f=inp.reduce(function(p,c,i){if(i%l==0){p.push([])}; p[p.length-1].push(c); return p}, [] );
										return f;
										}


	HAM_DST = function(a,b){return a.reduce(function(p,c,i){if(c!=b[i]){return p+1}else{return p}},0)};
	//reverse connection power, and force swarm to borrow bits from distanced particles
	NORMALIZE_DST = function(v){return (  (1/(1+Math.exp(-v)))  )}
	//NORMALIZE_DST = function(v){return (1-1/(1+Math.exp(-v)))*GSP_MLP}
	/**************************************************************************/
	/******************		UNUSED FOR A WHILE END	**************************/
	
	
	
	
	
	DEFAULT_SWARM_AUTOSIZE_F = function(v){return ~~(this.dim*0.2)}
	DEFAULT_STEPEND_CALLBACK = false;

	/*Some simple behavior profiles, to explore problems with unknown structure*/
	PS_ADVENTURERS = {lb:5,gb:2,inertia:0.5,g_max:0.7,nextGenPortion:20,size:0.7}
	PS_SOCIALISTIC = {lb:2,gb:6,inertia:1.5,g_max:0.99,nextGenPortion:80,size:0.2}
	PS_ANARCHISTIC = {lb:2,gb:2,inertia:3,g_max:0.8,nextGenPortion:90,size:1.5}
	PS_RGULAR	   = {lb:3,gb:5,inertia:1,g_max:0.99,nextGenPortion:90,size:0.25}

	/*
	p  - binary vector, represent state 
	dr - random component of movment
	dm - constant component of movement 
	*/
	function deriveDirection(p,dr,dm){
		var pat=0;
		directions = [];
		p.map(function(e,i){		
			var d=dm*dr;
			if(e==pat){directions.push({gd0:d,gd1:-d})}
			else{directions.push({gd0:-d,gd1:d})}	
		});
		return directions;
	}
	/*
	Swarm class, present parameters of discret optimization algorithm based on PSO(particle swarm optimization)
		and methods for set and solve optimization problem. Form of solution should be binary vector.
		Given function of solution fit(quality), used to estimate solution quality it's input
		should be binary array and expected output is numeric value of solution fit.
		Default direction of optimization is maximization of fit value (higher values 
		returned by fit function provide better quality of passed solution)
		
		size - swarm population size, could be either function (would be called with context of swarm, dim value
				is accessable on the moment of calling), or value. If value is float (contain dot) size=dim*value
				if integer size=value
		lb 	 - constant characteristic, defines how fast would swarm particle
				move around/to it's best known promlem solution
		gb 	 - constant characteristic, defines how fast would swarm particle
				move around/to swarm best known promlem solution
		inertia - 
				inertiaia of swarm particles, defines it's affection to stay in current states
		g_max - 
				swarm "density", when density of swarm exceed this value, population would
				be repalced by new random particles;
		nextGenPortion - 
				portion of particles to be replaced in case of swarm density exceed 
				defined minimum
		quality_better - 
				callback fuction defines which fit value from given 2 numerics is better, should return
				true when second argument passed to function is better then first. Default function
				IS_QUALITY_BETTER return true when second arg > first arg
		target_fit 	-  
				search would be stoped when model(solution) quality will exceed
				this value
		max_turns 	-  
				search would be stoped when number of epohs will exceed
				this value
		dim	-	problem dimention (the length of the binary vector representing 
				the problem solution)
		fit_fun	-	
				function evaluating model quality (input - binary vector, output - numeric value
				representing model quality)
		lmCallback - 
				function that would be called when swarm find next model quality maximum
		onDencityMax	- 
				function that would be called when swarm.gravity exceed swarm.g_max, after swarm reposition.
				By default it refresh swarm population with randomized particles calling DEFAULT_MAX_DENCITY_CALLBACK.
				Swarm object would be passed to callback as argument.
		onStepEnd	-
				faunction that would be called when one next step of algorithm is done. Swarm could be accesed inside function 
				through this.data
		bestSolution - 
				binary vector with best solution quality (best known solution)
		bestGF	- fit value related to best known solution
		bestGP	- best known solution(equal to bestSolution in regular case, if IS_MUTABLE
				= true, components of this vector may be randomly replaced, bestSolution is
				static and would not be changed while better solution not found)
		DeriveVel	-	
				acceleration(velosity) of moving to swarm best known state, identical
				for all particles, and defined by random component, gb, inertia and curent value of DeriveVel
				components
		gravity	-	value of swarm dencity 
		bees	-	array of swarm particles representing different solutios(binary vectors)
					and it's parameters

	*/
	function swarm(s){
		
		s=s||{};
		this.debugInf		={};
		
		this.dim			=s['dim']||DEFAULT_DIMENTIONS;
		if(s['size']){
		this.size=typeof (s['size']) == 'function' ? s['size'].call(this):((s['size'].toString().indexOf('.')>0)?(s['size']*this.dim):s['size']);}
		else{
				this.size=DEFAULT_SWARM_SIZE
		}
		
		this.lb				=s['lb']||DEFAULT_LOCAL_BEST_MPL;
		this.gb				=s['gb']||DEFAULT_GLOBSL_BEST_MPL;
		this.inertia		=s['inertia']||DEFAULT_MODEL_INERCIA
		this.g_max			=s['g_max']||DEFAULT_GRAVITY_MAX;
		
		this.target_fit		=s['target_fit']||DEFAULT_TARGET_FIT;
		this.max_turns		=s['max_turns']||DEFAULT_MAX_TURNS;
		this.fitFun			=s['fit_f']||DEFAULT_FIT_FUNCTION;
		this.lmCallback		=s['lmCallback']||DEFAULT_LMIN_CALLBACK;
		this.onDencityMax	=s['onDencityMax']||DEFAULT_MAX_DENCITY_CALLBACK;
		this.onStepEnd		=s['onStepEnd']||DEFAULT_STEPEND_CALLBACK;
		
		this.IS_MUTABLE		=s['is_mutable']==undefined?false:s['is_mutable'];
		this.USE_GOSSIPS	=s['use_gossip']==undefined?false:s['use_gossip'];
		
		this.gossip_pretender = false;
		this.nextGenPortion	=s['nextGenPortion']||DEFAULT_NEXTGEN_PORTION;
		this.quality_better =s['qualityCallback']||IS_QALITY_BETTER;
		
		this.bestSolution	=[];
		
		this.bestGF	=		0;
		this.bestGP	=		ARRAY_OF(this.dim,0);
		this.DeriveVel		=[];
		
		this.gravity		=0;
		
		this.bees			=[];
		this.addXBees(this.size);
		
		this.mapOf			=createFlatten.call(this,{
									col:~~Math.sqrt(this.dim),
									row:~~Math.sqrt(this.dim),
									cs:20});
		this.swarmAttractor();
		
		
		return this;
	}

	
	swarm.prototype.getSolutionSurface	=function(){
		var l = this.bees.length;
		var flat = this.bees.reduce(function(p,c){
				var cr=c.pos; 
				return p.map(function(e,i){return cr[i]+e});
			},
			ARRAY_OF(this.dim,0)).map(function(e){return e/l})
		//var l =~~Math.sqrt(l);
		var square =[]; //ARRAY_UNFLATTING(flat);
		return {'flat':flat,'square':square};
	}
	
	


	/*Reset behavior of swarm withowt affecting it's best known solution
		if input list contains specific key, corresponding parameter will be replaced by 
		value from this list.
	*/
	swarm.prototype.applyPreference		=function(s){
		s=s||{};
		if(s['size']){
				this.size=typeof (s['size']) == 'function' ? s['size']:((s['size'].toString().indexOf('.')>0)?(s['size']*this.dim):s['size']);
				this.bees			=[];
				this.addXBees(this.size);
		}

		if(s['relative_size']){
				this.size=~~(s['relative_size']*this.dim);
				this.bees			=[];
				this.addXBees(this.size);
		}
		
		this.lb				=s['lb']||this.lb;
		this.gb				=s['gb']||this.gb;
		this.inertia		=s['inertia']||this.inertia;
		this.g_max			=s['g_max']||this.g_max;
		this.nextGenPortion	=s['nextGenPortion']||this.nextGenPortion;
		this.onDencityMax		=s['onDencityMax']||this.onDencityMax;
		this.IS_MUTABLE		=s['is_mutable']==undefined?false:s['is_mutable'];
		this.USE_GOSSIPS	=s['use_gossip']==undefined?true:s['use_gossip'];
		
		return this;
	}

	/*Adds n bees with random positions and velosities to swarm */
	swarm.prototype.addXBees		=function(n){
		for(var i=0;i<n;i++){this.bees.push(new bee(this))};
		this.updateDistances();
		return this.bees;
	}
	/*Make n iterations(particles repositions) of optimization with termination criterias
	control, so serching would be stoped after exceeding target quality, or exceeding 
	iteration limit depending on what would happened first
		input
			n - number of iterations
		output
			bestGF - best known fit
	*/
	swarm.prototype.doSafeStep		=function(n){
		n=n||1;
		while((n>0)&&(this.max_turns>0)&&(!this.quality_better(this.target_fit,this.bestGF))){
			this.nextStep();
			n--;
		}
		console.log(this.bees);
		return this.bestGF;
	}

	/*Make n iterations(particles repositions) of optimization without break criteria
	control
		input
			n - number of iterations
		output
			bestGF - best known fit
	*/
	swarm.prototype.doStep		=function(n){
		n=n||1;
		var stps=n;
		while(n>0){
			this.nextStep();
			if(this.gravity>this.g_max){
				if(this.onDencityMax){this.onDencityMax(this)};
			}
			n--;
		}
		return this.bestGF;
	}

	

	/*Perform one step of optimization algorithm:
			-define parameters of bees aming to best known solution (attractive power of global
			optimum)
			-do reposition of all bees
			-get swarm dencity(gravity) after reposition
		input
			void
		output
			bestGF - best known fit
	*/
	swarm.prototype.nextStep  =function(){
				this.swarmAttractor();
				this.bees.map(function(e){return e.repos()});
				this.updateDistances();
				this.gravity=1-(this.bees.reduce(function(p,c,i){return p+c.cntDistance},0)/this.size);
				this.max_turns--;
				
				var m={data:{'swarm':this}};
					
				if(this.onStepEnd){	this.onStepEnd.bind(m).call();};	
				return this.bestGF;
		}
	


	
	/*Finding attractive power of global optimum, from random value, constant attractive power
	variable and current velosity of each component of optimal solution
		input
			void
		output
			DeriveVel - new values of global optimum attraction
	*/
	swarm.prototype.swarmAttractor		=function(){
		var dr=RND_VALUE(1);
		var dm=this.gb;
		this.DeriveVel=deriveDirection(this.bestGP,dr,dm);
		return this.DeriveVel;
		}

	/*Make randomized changes in best known solution (by inverting random components of
	it's binary vector), performed if IS_MUTABLE=true after swarm dencity exceed given limit,
	in most cases impairs optimization effectivenes, so not performed by default, and not
	recomended to use before all other preferences of algorithm was tuned. 
		input
			void
		output
			bestGP - randomized best known solution;
	*/
	swarm.prototype.coreMutation		=function(){
		//this.bestGP	=	this.bestGP.map(function(e,i){if(RND_VALUE(1)<MUTATION_PROB){return Math.abs(e-1)}return e})//RND_BINARY_ARRAY(this.dim,COMPONENT_PROB);
		this.bestGP	=RND_BINARY_ARRAY(this.bestGP.length)	//this.bestGP.map(function(e,i){if(RND_VALUE(1)<MUTATION_PROB){return Math.abs(e-1)}return e})//RND_BINARY_ARRAY(this.dim,COMPONENT_PROB);
		return this.bestGP;
		}
	/*Replace portion of swarm equal to n-% (n=100 - all bees would be replaced, n=50 - half bees population would be replaced...)
		with randomly created bees. n-% bees excluded from swarm selected randomly, independent from
		it's current fit or other characteristics.  
		input
			n - number of bees to be replaced with randomly created entity given in percents
		output
			bees - new swarm population with n% of new bees;
	*/
	swarm.prototype.nextGen			=function(n){
		var p=~~((this.size/100)*n);
		var numbers=this.bees.map(function(e,i){return i});
		while(p>0){
			var i = ~~RND_VALUE(numbers.length);
			var v = numbers[i];
			numbers.splice(i,1);
			this.bees[v]=new bee(this);
			p--;
		}
		if(this.IS_MUTABLE){this.coreMutation()};
		this.updateDistances();
		return this.bees;
	}	

	/* Swarm particle class (a.k.a. bee). Define structure of particle, contains solution,
		individual parameters of particle related to survey of optimal solution around 
		current position and methods to determine next position (value of solution), with
		individual parameters (current state of particle object) and preferences of algorithm
		stored in swarm
		
		parent_swarm - 
			point to swarm object which contain this bee in bees field 
		pos	- 
			binary array representing solution of discret optimization problem (position 
			of bee in multidimentional space of possible solutions)
		vel - 
			attractive power of local optimum (position with best solution quality
			ever found by this particle). Contain 2 arrays: 
					*probability of solution component to change from 1 to 0 (for each component)
					*probability of solution component to change from 0 to 1 (for each component)
		curentFit	-
			Current value of fit corresponding to current position (pos)
		
		cntDistance	-
			Distance from best solution ever found by all swarm (swarm.bestGP),
			value equal to hamming metric distance between bee position and swarm best known position
			divided by number of solution bits(length of binary vector). Used to find density of swarm
		
		bestLF	- best fit value ever found by this bee (best local fit)
		bestLP	- position coresponding to bestLF (best local position)
		
		Initial position defined by function RND_BINARY_ARRAY. It return binary vector, 
		of given length, with probability of "element=1" equal to given  COMPONENT_PROB.
		Initial velosity defined by INITIAL_VELOSITY() function which called for each componenet of 
		bee.vel arrays.
		*/
	function bee(c){
		this.parent_swarm		=c;
		this.pos				=RND_BINARY_ARRAY(c.dim,COMPONENT_PROB);
		this.vel				={selected_1:ARRAY_OF(c.dim,INITIAL_VELOSITY),ignored_0:ARRAY_OF(c.dim,INITIAL_VELOSITY)};
		this.gsp_attraction		={selected_1:ARRAY_OF(this.pos.length,0),ignored_0:ARRAY_OF(this.pos.length,0)};
		
		this.curentFit			=this.findFit();
		
		this.cntDistance		=1;
		
		this.bestLF				=this.curentFit;
		this.bestLP				=this.pos;

		this.hommieDst			=[];
		
		return this;
	}

	/*
	Calls fit function determined for swarm, with binary vector equals to it's pos.
	Update value of bee position fit with returned result. Compare quality of solution presented by
	bee current position with quality of it's best known position (stored in bestLP) and with swarm
	best known position stored in parent swarm bestGP field. Replace stored values and positions with
	new values if obtained fit of current bee.position better than stored. IS_QUALITY_BETTER function
	returns true if new_fit(second arg) better than new_fit (first arg). By default larger fit is better,
	which means that goal of optimization process is fit maximization. Setting custom callback may be used
	to change comparation logic 
	*/
	bee.prototype.findFit		=function(){
		var ps=this.parent_swarm;
		
		var m0={data:{'solution':this.pos}};
		this.curentFit=ps.fitFun.bind(m0).call();
		//ps.fitFun(this.pos);
		
		if(ps.quality_better(this.bestLF,this.curentFit)){
			this.bestLP=this.pos.map(function(e){return e});
			this.bestLF=this.curentFit;
		}
		if(ps.quality_better(ps.bestGF,this.curentFit)){

			ps.bestGP=this.pos.map(function(e){return e});
			ps.bestSolution=this.pos.map(function(e){return e});
			ps.bestGF=this.curentFit;
			
			var m1={data:{'swarm':ps}};
			ps.lmCallback.bind(m1).call();
		};
			
		return this.curentFit;
	}

	/*
	Update velocities of moving along each dimention with new values, for discret optimization problem
	 we use following definition of velocity: velocity of moving along dimention presented by 
	 coordinate C is probability of C to change it's current value ( let denote it as P(C->!C)) 
	 if C=0, velocity is P(C=1) at the next step, if C=1  velocity is P(C=0) at the next step.
	 In given algorithm, derivative velocities for step n+1 dv/dn=dv(n) defined by following equation
			dv(n+1)=normalize(w*v+f(gBest)+f(lBest))
		where   f(gBest)=randomValue*swarm.gb*dv(n) (dv(n) - swarm.DeriveVel at step n)
				f(lBest)=randomValue*swarm.gl*dv(n) ((dv(n) - bee.vel at step n))
		so derivative of velocity value is sum of
			(attraction to global best position (swarm.gb)*current velocity) scaled with random coeffitient 
			(attraction to local best position (swarm.lb)*current velocity) scaled with random coeffitient 
			component value scaled with swarm.inertia
		all values(v)  normalized by sigmoid function 
							
							____1____
								(-v)
							1+e
		function NORMALIZE define normalization rule(according to above mentioned formula by default).
		Return fit of current bee position (return value is never used).  
	*/


	bee.prototype.nextVelocity		=function(){
		var v1=this.vel.selected_1;
		var v0=this.vel.ignored_0;
		var lvd=this.beeAttractor();
		var ps =this.parent_swarm;
		var gvd=ps.DeriveVel;
		var w=ps.inertia;	
		
		if(ps.USE_GOSSIPS){
		/******************		UNUSED FOR A WHILE BEGIN	**************************/
		/**************************************************************************/
			/* Gossip feature is useless for now, and extremly impairs algorithm performance so USE_GOSSIPS property
			showld be setted to false during swarm object initialization. In such case code below would never be executed.
			*/
			var gspa0,gspa1;
			if (ps.gossip_pretender){
				gspa0 = this.gsp_attraction.ignored_0;
				gspa1 = this.gsp_attraction.selected_1;
			}	else {
				gspa1 = this.gsp_attraction.ignored_0;
				gspa0 = this.gsp_attraction.selected_1;
			}	
			this.vel.ignored_0=v0.map(function(e,i){return NORMALIZE(e*w+lvd[i].gd0+gvd[i].gd0+gspa0[i]) });
			this.vel.selected_1=v1.map(function(e,i){return NORMALIZE(e*w+lvd[i].gd1+gvd[i].gd1+gspa1[i]) });
		/******************		UNUSED FOR A WHILE END	**************************/
		/**************************************************************************/
			
			
		} else {
			this.vel.ignored_0=v0.map(function(e,i){return NORMALIZE(e*w+lvd[i].gd0+gvd[i].gd0) });
			this.vel.selected_1=v1.map(function(e,i){return NORMALIZE(e*w+lvd[i].gd1+gvd[i].gd1) });
		}
		return this.curentFit;
	}

	/*
	Update position of bee with new values (move swarm particle). Performed actions is:
			*refresh self velocities of moving along each coordinate (see bee.nextVelocity())
			*get new value of each coordinate. There are 2 types of new position:
					*(1)new position(value of solution component) may be equal to previous
					*(2)new position(value of solution component) may be not equal to previous
						(0 replaced by 1 or 1 replaced by 0) depends on old value
				to find type of new value position (1 or 2) let us compare velocity of moving along
				corresponding coordinate with random number(0..1). If random number is less than velocity
				we would invert old value (type 2), else value would not be changed (type 1).
				
				Underling probabilistic model is quite simple, if velosity of coordinate(c) change, defined as
				probability of iverting c value P(c->!c) (different velosities used for different direction(1->0 
				or 0->1) of change), than let us make c=!c for all particles on each step
				with this probabilities. 
				After reposition, let us update such probabilities with rules that makes
				values of "swarm best solution" and "bee best suolution" components more probable (for 
				all population) during next step repositioning.
				Repeat repositioning and P(c->!c) correction.
	*/
	bee.prototype.repos		=function(){
		this.nextVelocity();
		for(var i=0;i<this.pos.length;i++){
			var r = RND_VALUE(1);
			if((this.pos[i]==0)&&(r<this.vel.selected_1[i])){this.pos[i]=1;}
			if((this.pos[i]==1)&&(r<this.vel.ignored_0[i])){this.pos[i]=0;}
		}
		this.findFit();
		this.cntDistance=HAM_DST(this.pos,this.parent_swarm.bestGP)/this.pos.length;
		
		return this.pos;
	}

	
		/*
	Update attraction power of bee best solution for bee (higher values make particle move closer to 
	best position ever found by itself). Similar to swarm.swarmAttractor, manage attraction power of bee 
	best solution instead of attraction power of swarm best solution.
	*/
	bee.prototype.beeAttractor		=function(){
		var dr=RND_VALUE(1);
		var dm=this.parent_swarm.lb;
		return deriveDirection(this.bestLP,dr,dm);
	}

	this.swarm = swarm;
	this.DEFAULT_SWARM_AUTOSIZE_F = DEFAULT_SWARM_AUTOSIZE_F;
	this.DEFAULT_STEPEND_CALLBACK = DEFAULT_STEPEND_CALLBACK;
	this.arand			=				ARRAY_OF_RND_P;
	
	
	
	
	
	
	swarm.prototype.paramInfo		=function(){
		var s = "";
		var escape="\n";
		s+="dimention: "+this.dim+escape;
		s+="fit: "+this.bestGF+escape;
		//s+="solution: "+this.bestGP+escape;
		s+="steps total: "+this.debugInf.stepsTotal+escape;
		s+="details: "+escape+
			this.debugInf.steps.reduce(function(p,c,i,a){ 
				if(a[(i+1)%a.length].t==c.t){return p};
				return p+"turn: "+c.t+" fit: "+c.f+"| "+escape },"")
			+escape;
		return s;
	}
	
	
	
	
	
	
		/******************		UNUSED FOR A WHILE BEGIN	**************************/
	/**************************************************************************/
	
	swarm.prototype.resetGossip		=function(x){
		if(x==undefined){this.USE_GOSSIPS=!this.USE_GOSSIPS; return ("Gossip flag state: "+this.USE_GOSSIPS);}
		this.USE_GOSSIPS=x;return ("Gossip flag state: "+this.USE_GOSSIPS);
	}
	
	swarm.prototype.updateDistances		=function(){
		if(this.USE_GOSSIPS){
				this.bees.map(function(e,i){return e.fillHD(i)});
				this.bees.map(function(e,i){return e.getGossipAttractor(LOCAL_AREA_SIZE)});
				return 	this}
		return this;
	}

	
	var DERIVATIVE_ALG = function(a){
		return NORMALIZE(a.reduce(function(p,c,i){return p+c},0));
	}

					
	bee.prototype.getGossipAttractor		=function(area){
		
		var ini = {selected_1:ARRAY_OF(this.pos.length,-1),ignored_0:ARRAY_OF(this.pos.length,-1)};
		var hmd=this.hommieDst;
		var psb=this.parent_swarm.bees;
		//var locals=ARRAY_OF(area,0);
		var locals=ARRAY_OF(area,100);
		var i=0
		while(i<area){
			//if(locals.indexOf(hmd[i])<0){locals=locals.replaceLower(hmd[i]);} i++;
			//if(locals.indexOf(hmd[i]+RND_VALUE(1)+0.001)<0){locals=locals.replaceHigher(hmd[i]);} i++;
		//if(locals.indexOf(hmd[i])<0){
			locals=locals.replaceHigher(hmd[i]);
			//locals=locals.replaceLower(hmd[i]);
			//} 
			i++;
			}
		locals = locals.reduce(function(p,c){var ind=hmd.indexOf(c);if(ind<0){return p} return p.concat(ind) },[]);
	
		this.gsp_attraction=locals.reduce(function(p,c,i,a){
			var n=c;
			//console.log(i);
			var cft=NORMALIZE(psb[c].bestLF)
			p.selected_1=psb[c].vel.selected_1.map(function(e,index){return NORMALIZE(hmd[n]*e*cft*GSP_MLP + p.selected_1[index])})
			p.ignored_0=psb[c].vel.ignored_0.map(function(e,index){return NORMALIZE(hmd[n]*e*cft*GSP_MLP + p.ignored_0[index])})
			return p;
		},ini);
		
		/*
		this.gsp_attraction=this.parent_swarm.bees.reduce(
			function(p,c,i){
				//c.vel.selected_0;
				p.selected_1=c.vel.selected_1.map(function(e,ind){return (e*hmd[i] + p.selected_1[ind])/2});
				p.ignored_0=c.vel.ignored_0.map(function(e,ind){return (e*hmd[i] + p.ignored_0[ind])/2});
				//console.log(p);
			return p;
		},ini);*/
	}

		
	bee.prototype.fillHD = function(n){
		var ps = this.parent_swarm;
		var ps_bees=ps.bees;
		var tail = ps.bees.slice(n,ps_bees.length);
		var cpos = this.pos;
		this.gossipTalkingDV=[];

		var tail_prc = tail.map(function(e,i){ 
							var d = HAM_DST(e.pos,cpos)/cpos.length; 
							d=NORMALIZE_DST(d);
							 ps.bees[i+n].hommieDst[n]=d; ps.bees[n].hommieDst[i+n]=d;return d }
							);
		//this.hommieDst = this.hommieDst.concat(tail_prc.slice(1,tail_prc.length));
	}
	
	
		/*
	bee.prototype.nextVelocity		=function(){
		var v1=this.vel.selected_1;
		var v0=this.vel.ignored_0;
		var lvd=this.beeAttractor();
		var gvd=this.parent_swarm.DeriveVel;	
		var w=this.parent_swarm.inertia;	
		
		this.vel.ignored_0=v0.map(function(e,i){return NORMALIZE(e*w+lvd[i].gd0+gvd[i].gd0) });
		this.vel.selected_1=v1.map(function(e,i){return NORMALIZE(e*w+lvd[i].gd1+gvd[i].gd1) });
		
		return this.curentFit;
	}*/
		/******************		UNUSED FOR A WHILE END	**************************/
	/**************************************************************************/


};

pso._construct();

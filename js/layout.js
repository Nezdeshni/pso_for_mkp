var PADDING_MASK			="0000000000000000000000000000";
var PADDING_VALUE			=PADDING_MASK.substring(0,20);
String.prototype.paddingLeft 	=function(paddingValue){return String(paddingValue + this).slice(-paddingValue.length)};
Array.prototype.replaceLower = function(x){
		return this.reduce(function(p,c,i,a){
				if(c<p.r){p.ar=a;	p.ar[i]=p.r;	return {r:c,ar:p.ar}} return p;},{r:x,ar:this}).ar}

Array.prototype.replaceHigher = function(x){
		return this.reduce(function(p,c,i,a){
				if(c>p.r){p.ar=a;	p.ar[i]=p.r;	return {r:c,ar:p.ar}} return p;},{r:x,ar:this}).ar}
	

	ARRAY_OF = function(len,v){return Array.apply(null, {length: len}).map(
						function(){return typeof (v) !== 'function' ? v:v.call(this)})};
						
						
	//Return random float number in range 0..VALUE
	RND_VALUE = function(VALUE){return Math.random()*VALUE};
	//Return value 1 with probability p, or value 0 with probability 1-p
	GET_RELIZATION = function(p){return RND_VALUE(1)<p ? 1:0};
	//Return array of length len filled by binary digits. Each digit is randomly generated, probability of value 1 is equals to p;
	RND_BINARY_ARRAY = function(len,p){return Array.apply(null, {length: len}).map(function(){return GET_RELIZATION(p)})};
	ARRAY_OF_RND_P = function(len,p){return Array.apply(null,{length:len}).map(function(){return RND_VALUE.call(1,p)})};


function IS_QALITY_BETTER(old_fit,new_fit){return old_fit<new_fit}


function fit_max_cost(){
	
	var f=kp.estimateSolution(this.data.solution);
	if(!f){return 0};
	return f;
}

function fit_max_stuff(s){
	return kp.state.reduce(function(p,c){return p+c},0);
}

var kp;
var swarmObject;

var story={sq:[],flatSteps:[],n:0,i:0,sf:{},tsLabel:"flatSteps",
			dim:function(){var b=~~Math.sqrt(this.sf[0]?this.sf[0].length:0);
							
						return b^2<=this.sf.length?{col:b,row:(b+1)}:{col:b,row:b}},
			cs:30,
			clean:function(){this.flatSteps=[];this.sq=[];this.n=0;this.i=0;this.sf={}},
			flatSuccess:[],
			setTs:function(x){this.tsLabel=x||"flatSteps"; return this}
			};
			
var storyP = function(s){return onStep}


function MAX_DENCITY_CALLBACK(){
		//console.log("next gen: g= "+s.gravity);
		data.swarm.nextGen(s.nextGenPortion);
		return data.swarm;
	}
const DEFAULT_MAX_TURNS=64000;
function refreshfitlist(fit){
	$("#fitlist").append(`<li>${kp.estimateSolution(fit)} (${kp.estimateSolution(fit)-kp.estimateSolution(story.flatSuccess.slice(-2)[0])} -> )</li>`)
}
function LMIN_CALLBACK(){
		//console.log(this)
		var swrm = this.data.swarm;
		swrm.debugInf.steps=swrm.debugInf.steps||[];
		swrm.debugInf.steps.push({t:(DEFAULT_MAX_TURNS-swrm.max_turns),f:swrm.bestGF})
		swrm.debugInf.stepsTotal = swrm.debugInf.steps.length;
		story.flatSuccess.push(swrm.bestGP)
		refreshfitlist(story.flatSuccess.slice(-1)[0])
		//console.log((DEFAULT_MAX_TURNS-s.max_turns)+": "+s.bestGF+" /g: "+s.gravity)
	}

var onStep= function(){
	var swrm = this.data.swarm;
	var flatten=swrm.getSolutionSurface().flat;
	story.flatSteps.push(flatten);
	return story.sq;
	}


var HEAVY_ENVIRONMENT = {
	AMT:		{m:47,v:64,p:40,a:50,b:20,r:30,t:60,y:35,u:30},
	averAMT:	{m:6,v:8,p:5,a:8,b:3,r:3,t:5,y:3,u:4},
	stuffNum:	100,
	//kanspak:	kp,
	avgProfit:  100,
	swarmSet:	LIGHT_EXPLORE
	}

var STANDART_ENVIRONMENT = {
	AMT:		{m:47,v:64,p:40,a:50,b:20,r:30,t:60,y:35,u:30},
	averAMT:	{m:6,v:8,p:5,a:8,b:3,r:3,t:5,y:3,u:4},
	stuffNum:	225,
	dataKey:	"225",
	//kanspak:	kp,
	avgProfit:  100,
	swarmSet:	SHORT_EXPLORE	
	/*{dim:225,
				fit_f:fit_max_cost,
				size:49,
				onStepEnd:storyP(story),
				lmCallback:LMIN_CALLBACK
	
	}*/
}

try {
	var GOSSIP_TEST_SET = {onStepEnd: storyP(story), rep: 10, steps: 10, dataKey: '200x', size: 0.2, stuffNum: 200}
	var REGULAR_TEST_SET = {
		onStepEnd: storyP(story),
		rep: 10,
		steps: 10,
		dataKey: '200x',
		size: pso.DEFAULT_SWARM_AUTOSIZE_F || -1,
		stuffNum: 200
	}
	var SHORT_TEST_SET = {
		onStepEnd: storyP(story),
		rep: 10,
		steps: 10,
		dataKey: '20x',
		size: pso.DEFAULT_SWARM_AUTOSIZE_F || -1,
		stuffNum: 20
	}
} catch (e){
	console.error(e);
	console.warn('env would not be initialized properly!')
	GOSSIP_TEST_SET={};
	SHORT_TEST_SET={};
	REGULAR_TEST_SET={};
}
var SHORT_EXPLORE = {
								rep:1,steps:100,
								dataKey:'64',
								dim:64,
								size:4,
								//stuffNum:225,
								stuffNum:64,
								fit_f:fit_max_cost,
								onStepEnd:onStep,
								lmCallback:LMIN_CALLBACK
								}
var LIGHT_EXPLORE = {
								rep:1,steps:1000,
								dataKey:'225',
								dim:100,
								size:49,stuffNum:100,
								fit_f:fit_max_cost,
								onStepEnd:onStep,
								lmCallback:LMIN_CALLBACK
								}


	

function setEnv(x){
	x=x||{};
	var AMT = 	  x['AMT']||{m:47,v:64,p:40,a:50,b:20,r:30,t:60,y:35,u:30};
	var averAMT = x['averAMT']||{m:6,v:8,p:5,a:8,b:3,r:3,t:5,y:3,u:4};
	var stuffNum= x['stuffNum']||100;
	//var KanspakObj= x['kanspak']||kp;
	var averProfit =x['avgProfit']||100;
	var swarmS 	   =x['swarmSet']||SHORT_EXPLORE;//{dim:stuffNum,fit_f:fit_max_cost,size:20};
	var	key		=x['dataKey']||'324';
	
	kp=new Kanspak({amount:AMT});
	kp.randomItems(stuffNum,averAMT,averProfit);
	
	//kp=new Kanspak({amount:AMT});
	//kp.loadGivenSet(key);
	//console.log(arguments)
	//console.dir(x);
	//swarmObject = new pso.swarm(swarmS);
	return true//swarmObject.bestGF;
}
function storeEnv(key,r){
	console.assert(localStorage[key]&&r,'env is already exist, use storeEnv(key,true) to overwrite!')
	
	kp.saveGivenSet(key);
	if(localStorage[key]){
		console.log('env saved sucessfull!')
	} else {
		console.log('env was not created')
	}
	return kp;
}
function restoreEnv(key){
	kp=new Kanspak();
	return kp.loadGivenSet(key);
}


function makeSerialTest(setting){
	var escape="\n";
	var summary={avgT:0,avgF:0,steps:[],text:""};
	console.time("Execution time:");
	
	for (var i=0;i<setting.rep;i++){
		//console.dir(restoreEnv(setting.dataKey));
		restoreEnv(setting.dataKey);
		setting.stuffNum=kp.content.length;
		
		swarmObject = new pso.swarm(
			{	size:setting.size,
				dim:setting.stuffNum,
				fit_f:fit_max_cost,
				onStepEnd:setting.onStepEnd
				
			});
			//console.dir(swarmObject);
		swarmObject.doStep(setting.steps);
		s=swarmObject.debugInf.steps.reduce(function(p,c,o,a){ 
			if(a[(o+1)%a.length].t==c.t){return p};
			return p+"turn: "+c.t+" fit: "+c.f+"| " },"")
		+escape;

		var stp=swarmObject.debugInf.steps;
		summary.text+="turn: "+stp[stp.length-1].t+" fit: "+stp[stp.length-1].f+"| "+escape;
		summary.avgT+=stp[stp.length-1].t;
		summary.avgF+=stp[stp.length-1].f;	
		summary.steps.push(stp);
	}
	console.timeEnd("Execution time:");
	summary.avgT/=setting.rep;
	summary.avgF/=setting.rep;
	console.log("average: "+"turn: "+summary.avgT+" fit: "+summary.avgF+"| "+escape)
	//console.dir(summary);
	return summary;
}


function directStateRoullete(){
	n=Math.pow(2,kp.state.length);
	console.log(n);
	var bp=[];var bf=0;var inv=n;
	while(n>0){
		var es =kp.estimateSolution(n.toString(2).paddingLeft(PADDING_VALUE).split('').map(function(e){return parseInt(e)})); 
		if(IS_QALITY_BETTER(bf,es)){
			bf=kp.goods;
			bp=kp.state.map(function(e){return e});
			console.log((inv-n)+": "+bf)
		}
		n--;
	}
	console.log(bp);
	return bf;
}


function randomStateRoullete(n){
	n=n||1;
	var bp=[];var bf=0;	var inv=n;
	while(n>0){
		if(IS_QALITY_BETTER(bf,kp.randomState())){
			bp=kp.state.map(function(e,i){return e});
			console.log((inv-n)+": "+bf)	
		}
		n--;
	}
	console.log(bp);
	return bf;
}

var mapOfSurf;

var do_layout = function(dataSetting){
	story.clean();
	story.tsLabel=null;
	swarmObject=null;
	kp=null;
	
	//setEnv();
	//setEnv(HEAVY_ENVIRONMENT);
	//swarmObject = new pso.swarm(LIGHT_EXPLORE);
	
	//swarmObject = new pso.swarm(LIGHT_EXPLORE);
	//setEnv(STANDART_ENVIRONMENT);
	
	setEnv( LIGHT_EXPLORE);
	swarmObject = new pso.swarm(LIGHT_EXPLORE);
	

	swarmObject.IS_MUTABLE=dataSetting.mutable;

	swarmObject.doStep(120);
	mapOfSurf.setData(story.setTs('flatSteps'));
	//mapOfSurf.setData(story.setTs('flatSuccess'));
	mapOfSurf.anim({inf:false,duration:50});
	/*
	var x=story.n
	
	story.i=(story.i+1)%x;
	swarmObject.mapOf.rcolor(story.sf[story.i])
*/
	console.log(swarmObject.bestGF);
}

var search_again = function(){
	swarmObject.doStep(120);
}
var replay_map = function(){
	swarmObject.doStep(120);


	story.flatSteps=story.flatSteps.slice(-120);
	story.sf=story.sf.slice(-120);
	mapOfSurf.setData(story.setTs("flatSteps"));
	mapOfSurf.anim({inf:false,duration:20});

}
var any_action = function(){}

$(document).ready(function(){
	//pso_view._construct();
	//pso_view.
	mapOfSurf=createFlatten();
	$("#rndLInt").on('click', do_layout);
	$("#send_them_btn").on('click',search_again);
	$("#replay_btn").on('click',replay_map)
	$("#any_btn").on('click',any_action)

	
});
